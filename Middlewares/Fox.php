<?php

namespace Lvzmen\Apifox\Middlewares;

use Closure;
use Illuminate\Support\Facades\Config;
use Lvzmen\Apifox\Doc;

/**
 * how to user:
 * php artisan apifox {table}
 *
 * Class Fox
 * @package App\Http\Middleware
 */
class Fox
{
    /**
     * 处理传入的请求。
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    /**
     * 在响应发送到浏览器后处理任务。
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Http\Response $response
     * @return void
     */
    public function terminate($request, $response)
    {
        if (Config::get('app.env') == 'local') {
            (new Doc())->saveToFile($request, $response);
        }
    }
}
