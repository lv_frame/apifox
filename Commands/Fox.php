<?php

namespace Lvzmen\Apifox\Commands;

use Lvzmen\Apifox\Doc;
use Illuminate\Console\Command;

class Fox extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apifox {table}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Doc For Apifox';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $table = $this->argument('table');
        $table = $table ?? '';
        (new Doc($table))->createApi();

        return 0;
    }
}
