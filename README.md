# Apifox文档自动生产工具

开发这个工具的主要解决的问题：
- 在手动创建apifox接口文档的时候，每个接口的字段都需要从数据表中获取字段说明，大量复制粘贴很麻烦；
- 导入json格式的响应时，会覆盖原来就存在的字段说明；


开发思路：  
将每次请求和响应都保存在一个临时文件中，通过命令行触发apifox文档创建请求，解析临时文件中的内容，利用apifox提供的openapi接口，向指定项目自动创建文档。

[apifox的openapi文档](https://apifox-openapi.apifox.cn/api-48643958)

## 1、安装
```shell
composer require lvzmen/apifox
```

## 2、配置
在laravel中的`.env`文件中引入如下配置：
```shell
APIFOX_PROJECT_ID=21***69
APIFOX_TOKEN=APS-PbMdZ0nu************UQu9nZKNbtfB
APIFOX_FILE_PATH=/tmp/api_fox.cnf
APIFOX_DB_CONNECTION=mysql
APIFOX_DB=forge
```
- `APIFOX_PROJECT_ID`：在apifox中对应的项目id，详情请看[官网](https://www.apifox.cn/help/openapi/#%E4%B8%AA%E4%BA%BA-api-%E8%AE%BF%E9%97%AE%E4%BB%A4%E7%89%8C);
- `APIFOX_TOKEN`：需要自己生成一个唯一的api请求token，如何获取请看[官网](https://www.apifox.cn/help/openapi/#%E4%B8%AA%E4%BA%BA-api-%E8%AE%BF%E9%97%AE%E4%BB%A4%E7%89%8C);
- `APIFOX_FILE_PATH`：这是一个临时文件，用于保存每次请求和响应，作为原始数据发送到apifox；
- `APIFOX_DB_CONNECTION`：数据库链接名称，应该在`config/database.php`文件中获取；
- `APIFOX_DB`：要操作的数据库，从这个库中获取字段的说明；


## 3、如何使用

### 3.1 添加命令
一般通过命令行的方式触发文档生产功能，因此需要向`Laravel`中添加命令：
`app/Console/Kernel.php`:
```php
namespace App\Http;
use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    protected $middlewareGroups = [
        'api' => [
            \Lvzmen\Apifox\Commands\Fox::class
        ],
    ];
}
```

### 3.2 添加中间件
这个中间件的目的就是拦截每次请求和响应，将这些数据保存到临时文件中：
`app/Http/Kernel.php`:app/Console/Kernel.php

```php
<?php

namespace App\Console;

use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        \Lvzmen\Apifox\MiddleWares\Fox::class
    ];
}
```

### 3.3 配置自定义字段
如果有些字段无法从数据库中获取字段说明，则需要在`database/data/apifox.json`文档中设置字段：
```json
{
    "v1": {
        "name": "乡镇名称",
        "town_code": {
            "type": "string",
            "desc": "镇编码"
        },
        "village_code": {
            "type": "string",
            "desc": "村编码"
        },
        "wd": {
            "type": "string",
            "desc": "纬度"
        },
        "jd": {
            "type": "string",
            "desc": "经度"
        },
        "id": {
            "type": "integer",
            "desc": "主键"
        },
        "code": {
            "type": "string",
            "desc": "乡镇编码"
        }
    }
}
```

### 3.4 创建文档
在命令行中执行如下命令，可以根据临时文件中的请求和响应创建apifox的接口文档：
```shell
# 使用数据库中table_name表的字段说明，但是不采用自定义的字段说明
php artisan apifox table_name

# 使用在apifox.json中的字段说明，但是不使用数据库表字段说明
php artisan apifox v1

# 使用数据库表的字段说明，但是会被v1里的说明覆盖
php artisan apifox table_name.v1
```

## 4、待解决问题
- 现在apifox的openapi只提供了创建文档的接口，没有提供更新和删除的接口，因此如果对同一个url发起多次文档创建，则不会生效；
- 如果在apifox中已经存在了相同url的接口，则不会创建新的接口；